{
  description = "A minimal example of a nix flake for developing a python package";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-21.11;

  outputs = { self, nixpkgs }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
      deps = [ pkgs.python3Packages.numpy ];
    in rec {
      # define the python package
      packages.x86_64-linux = {
        simple-example = pkgs.python3Packages.buildPythonPackage {
          pname = "simple-example";
          version = "0.1";
          src = self;
          propagatedBuildInputs = deps;
        };
      };

      defaultPackage.x86_64-linux = packages.x86_64-linux.simple-example;

      # define a shell environment - useful for package development
      devShell.x86_64-linux = pkgs.mkShell {
        name = "simple-example-dev-shell";
        buildInputs = [
          (pkgs.python3.withPackages(ps: deps))
        ];
      };
    };
}
