# Simple package

A minimal example of setting up a python package as a nix flake.

* creates a development shell for use in testing/running the package for development.
* exports a python package, for use as a dependency elsewhere.

To run the development shell use `nix develop`. Run the example with `python3 -m simple-package.hello` to see "Hello World!" printed.

